#include <cassert>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <stop_token>
#include <thread>
#include <vector>

static std::vector<int> data;
static std::mutex mutex;
static std::condition_variable conditionVariable;

void print(std::stop_token stopToken, std::size_t index)
{
	while (!stopToken.stop_requested())
	{
		std::unique_lock lock{mutex};
		conditionVariable.wait(lock, [stopToken] { return stopToken.stop_requested() || !data.empty(); });
		if (stopToken.stop_requested())
			return;
		assert(!data.empty());
		std::cout << "Thread " << index << ":\t" << data.back() << '\n';
		data.pop_back();
	}
}

int main()
{
	const unsigned int threadCount = std::max(2u, std::jthread::hardware_concurrency());
	std::cout << "Running " << threadCount << " threads.\n";

	std::vector<std::jthread> threads;
	threads.reserve(threadCount);

	for (unsigned int i = 0; i < threadCount; ++i)
		threads.emplace_back(print, i + 1);

	for (int input; std::cin >> input && 0 != input;)
	{
		{
			std::lock_guard lock{mutex};
			data.push_back(input);
		}
		conditionVariable.notify_one();
	}

	for (std::jthread& thread : threads)
		thread.request_stop();

	conditionVariable.notify_all();

	return 0;
}
