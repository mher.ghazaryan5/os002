#include <cassert>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <stop_token>
#include <string>
#include <thread>

static std::string data;
static std::mutex mutex;
static std::condition_variable conditionVariable;

void print(std::stop_token stopToken)
{
	while (!stopToken.stop_requested())
	{
		std::unique_lock lock{mutex};
		conditionVariable.wait(lock, [stopToken] { return stopToken.stop_requested() || !data.empty(); });
		if (stopToken.stop_requested())
			return;
		assert(!data.empty());
		std::cout << data << '\n';
		data.clear();
	}
}

int main()
{
	std::jthread thread{print};
	for (std::string input; std::cin >> input && "exit" != input;)
	{
		{
			std::lock_guard lock{mutex};
			data = input;
		}
		conditionVariable.notify_one();
	}
	thread.request_stop();
	conditionVariable.notify_all();
	return 0;
}
